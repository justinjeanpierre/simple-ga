import Foundation

print("Simple optimization using a genetic algorithm.\n")

// datatypes
enum Crossover {
	case midpoint, uniform, twopoint
}

// functions
func fitness(chromosome: [Int]) -> Int {
	//	fitness function: (a+b) - (c+d)
	return (chromosome[0] + chromosome[1]) - (chromosome[2] + chromosome[3])
}

func crossoverParents(parent1: [Int], parent2: [Int], cross: Crossover? = .midpoint) -> ([Int], [Int]) {
	// assumes length of parent1, parent2 is 4
	// (todo: check length of inputs, crossover accordingly)
	var child1: [Int]
	var child2: [Int]
		
	/// switch based on Crossover.case
	switch(cross) {
		case Crossover.uniform?: 
			child1 = [parent1[0], parent2[1], parent1[2], parent2[3]]
			child2 = [parent2[0], parent1[1], parent2[2], parent1[3]]
		
		case Crossover.twopoint?: 	
			child1 = [parent1[0], parent2[1], parent2[2], parent1[3]]
			child2 = [parent2[0], parent1[1], parent1[2], parent2[3]]
		
		default: // midpoint
			// single-point crossover function in
			// the middle of the chromosome
			child1 = [parent1[0], parent1[1], parent2[2], parent2[3]]
			child2 = [parent2[0], parent2[1], parent1[2], parent1[3]]
	}

	return (child1, child2)
}

func iterateWithCrossoverForPopulation(inout pop: [[Int]]) {
	print("Population before crossover: \(pop)")

	print("Starting fitness for x1 = \(pop[0]) is \(fitness(pop[0])).")
	print("Starting fitness for x2 = \(pop[1]) is \(fitness(pop[1])).")
	print("Starting fitness for x3 = \(pop[2]) is \(fitness(pop[2])).")
	print("Starting fitness for x4 = \(pop[3]) is \(fitness(pop[3])).")

	let child1 = crossoverParents(pop[0], parent2:pop[1])
//	let child1 = crossoverParents(pop[0], parent2:pop[1], cross: .uniform)
	pop.append(child1.0)
	pop.append(child1.1)

	let child2 = crossoverParents(pop[2], parent2:pop[3])
//	let child2 = crossoverParents(pop[2], parent2:pop[3], cross: .uniform)
	pop.append(child2.0)
	pop.append(child2.1)

	// reorder population
	pop.sortInPlace({ fitness($0) > fitness($1) })
	
	// population now contains twice as many elements.
	// delete the second half of the array
	for _ in 0..<((pop.count)/2) {
		pop.removeLast()
	}
	
	print("Population after crossover: \(pop)")
	
	print("New fitness for x1 = \(pop[0]) is \(fitness(pop[0])).")
	print("New fitness for x2 = \(pop[1]) is \(fitness(pop[1])).")
	print("New fitness for x3 = \(pop[2]) is \(fitness(pop[2])).")
	print("New fitness for x4 = \(pop[3]) is \(fitness(pop[3])).")
	print()
}

// main program

// starting chromosome: x = abcd
let x1 = [1, 2, 3, 4]
let x2 = [5, 6, 7, 8]
let x3 = [8, 7, 6, 5]
let x4 = [4, 3, 2, 1]

var population = [x1, x2, x3, x4]

for i in 0..<3 {
	iterateWithCrossoverForPopulation(&population)
}
